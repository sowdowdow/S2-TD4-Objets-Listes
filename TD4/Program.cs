﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TD4
{
    class Program
    {
        static void Main(string[] args)
        {
            bool fin = false;
            string choix = null;
            //instanciation des classes IHM et Methodes
            Liste_camions liste_camions = new Liste_camions("camions.txt", '_');

            do
            {
                Console.WriteLine("Choisissez une action (Ajouter,Afficher,Rechercher,Supprimer,Quitter) :");
                choix = Console.ReadLine().ToLower().Trim(' ');
                //On efface la console (pour un affichage plus propre)
                Console.Clear();
                switch (choix)
                {
                    case "afficher":
                        Console.Clear();
                        liste_camions.Affichage();
                        break;
                    case "ajouter":
                        Console.Clear();
                        liste_camions.Ajout();
                        break;
                    case "rechercher":
                        liste_camions.Recherche();
                        break;
                    case "supprimer":
                        Console.Clear();
                        liste_camions.Suppression();
                        break;
                    case "quitter":
                        fin = true;
                        break;
                    default:
                        Console.WriteLine("**Erreur de saisie.");
                        break;
                }
            } while (fin == false);
        }
    }
}
