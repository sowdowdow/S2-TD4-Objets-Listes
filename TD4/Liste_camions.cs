﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace TD4
{
    public class Liste_camions
    {
        //nom du fichier de sauvegarde
        private string Nom_Fichier;
        //Liste des objets camions
        public List<Camion> liste_camions = new List<Camion>();
        //caractere separateur
        private char Separateur;

        //Constructeur prenant en argument l'adresse du fichier
        public Liste_camions(string adresse_fichier, char carac_separateur)
        {
            Nom_Fichier = adresse_fichier;
            Separateur = carac_separateur;
            Chargement_Memoire();
        }


        //Destructeur (il est appelé automatiquement)
        ~Liste_camions() 
        {
            Sauvegarde_Memoire();
            //pause pour voir l'affichage avant la fermeture du programme
            System.Threading.Thread.Sleep(1000);
        }


        #region methodes
        public void Ajout()
        {
            int useless; //utilisé pour vider les TryParse
            string[] attributs_nouveau_camion = new string[5];

            Console.WriteLine(nameof(Camion.Marque) + " : ");
            attributs_nouveau_camion[0] = Console.ReadLine();

            Console.WriteLine(nameof(Camion.Type) + " : ");
            attributs_nouveau_camion[1] = Console.ReadLine().ToUpper();

            //tant que la DATE saisie n'est pas comprise entre :        Maintenant et 1700
            //et que le texte saisi n'est pas un entier
            do
            {
                Console.WriteLine(nameof(Camion.Annee_Fabrication) + " : ");
                attributs_nouveau_camion[2] = Console.ReadLine();
            } while (!int.TryParse(attributs_nouveau_camion[2], out useless) || int.Parse(attributs_nouveau_camion[2]) < 1700 || int.Parse(attributs_nouveau_camion[2]) > DateTime.Now.Year);

            //tant que la saisie n'est pas un chiffre supérieur à 0
            do
            {
                Console.WriteLine(nameof(Camion.Puissance_moteur) + " : ");
                attributs_nouveau_camion[3] = Console.ReadLine();
            } while (!int.TryParse(attributs_nouveau_camion[3], out useless) || int.Parse(attributs_nouveau_camion[3]) < 0);

            //tant que la saisie n'est pas un chiffre supérieur à 0
            do
            {
                Console.WriteLine(nameof(Camion.Prix_vente) + " : ");
                attributs_nouveau_camion[4] = Console.ReadLine();
            } while (!int.TryParse(attributs_nouveau_camion[4], out useless) || int.Parse(attributs_nouveau_camion[4]) < 0);

            //AJOUT du nouveau camion dans la liste
            liste_camions.Add(new Camion(attributs_nouveau_camion));


            //Affichage de l'ajout
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("+++++");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Ajout " + attributs_nouveau_camion[0] + " " + attributs_nouveau_camion[1]);
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("+++++");
            Console.ForegroundColor = ConsoleColor.White;
        }

        public void Chargement_Memoire()
        {
            try
            {
                StreamReader fichier = new StreamReader(Nom_Fichier);
                string str;
                string[] caracteristiques = new string[5];
                //si la ligne actuelle n'est pas vide
                while ((str = fichier.ReadLine()) != null)
                {
                    //On initialise un tableau (de string) qui contient la chaîne découpée
                    caracteristiques = str.Split(Separateur);
                    //puis on initialise une nouvelle instance de camion
                    liste_camions.Add(new Camion(caracteristiques[0], caracteristiques[1], caracteristiques[2], caracteristiques[3], caracteristiques[4]));




                    /*
                     *Sous-chaîne allant de 0 au 2eme
                     * caractere '_' rencontré
                     */
                }
                fichier.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("erreur de lecture du fichier :\n========================\n" + e);
            }
        }

        public void Recherche()
        {
            int prix_max = 0;


            //saisie du prix maximum
            Console.WriteLine("Prix maximum : ");
            try
            {
            prix_max = int.Parse(Console.ReadLine());
            }
            catch (Exception e)
            {
                Console.WriteLine("/!\\ Erreur : "+e.Message);
            }
            

            //Si un camion est trouvé, on passe à la suite, sinon on continue
            bool camion_trouve = false;
            int nb_camions = 0;
            foreach (var camion in liste_camions)
            {
                if (camion.Prix_vente < prix_max)
                {
                    camion_trouve = true;
                    nb_camions++;
                }
            }
            if (camion_trouve == true)
            {
                Console.Clear();
                //Affichage des camion trouvés
                Console.WriteLine(nb_camions +" Camion(s) dont le prix de vente est inférieur " + prix_max + " : \n");
                foreach (var camion in liste_camions)
                {
                    if (camion.Prix_vente < prix_max)
                    {
                        camion.Affiche();

                    }
                }
            }
            else
            {
                Console.WriteLine("Aucun camion trouvé a moins de " + prix_max + ".");
            }
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("======================");
            Console.ForegroundColor = ConsoleColor.White;
        }

        public void Sauvegarde_Memoire()
        {
            try
            {
                //On instancie un StreamWriter qui va permettre d'écrire dans un fichier texte
                using (StreamWriter fichier = new StreamWriter(Nom_Fichier))
                {
                    string chaine_sauvegarde;
                    foreach (var camion in liste_camions)
                    {
                        chaine_sauvegarde = camion.Marque + Separateur + camion.Type + Separateur + camion.Annee_Fabrication.Year + Separateur + camion.Puissance_moteur + Separateur + camion.Prix_vente;
                        fichier.WriteLine(chaine_sauvegarde);
                    }
                }
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write("sauvegarde :   OK");
                Console.ForegroundColor = ConsoleColor.White;
            }
            catch (Exception)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("Echec sauvegarde /!\\ ");
                Console.ForegroundColor = ConsoleColor.White;
            }
        }

        public void Suppression(params int[] liste_ID_affichage) //Le premier camion est N°0 /!\
        {
            //tri du tableau par ordre décroissant pour éviter la propagation de changement d'ID dans la liste de camion
            liste_ID_affichage = liste_ID_affichage.OrderByDescending(e => e).ToArray();


            //suppression de chaque camion
            foreach (var ID_Camion in liste_ID_affichage)
            {
                try
                {
                    string visuel = liste_camions[ID_Camion].Marque + " " + liste_camions[ID_Camion].Type + "\tCamion n°" + ID_Camion + " supprimé.";
                    liste_camions.RemoveAt(ID_Camion);
                    Console.WriteLine(visuel);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Erreur de suppression du camion n°" + ID_Camion + " :\n" + "**************\n" + e.Message + "\n**************");
                }
            }
        }

        public void Affichage()
        {
            //string[]     
            foreach (var camion in liste_camions)
            {
                //ligne de séparation colorée
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("======================");
                Console.ForegroundColor = ConsoleColor.White;

                //N° du camion
                Console.Write(liste_camions.IndexOf(camion) + " : ");
                camion.Affiche();
            }
            //ligne de fin
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("======================");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Total : "+liste_camions.Count+" Camion(s)");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("======================");
            Console.ForegroundColor = ConsoleColor.White;
        }
        #endregion methodes

    }
}