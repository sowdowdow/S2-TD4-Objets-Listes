﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TD4
{
    public class Camion
    /*
    * Composition d'un camion :
    * Marque
    * Type
    * Annee_Fabrication
    * PuissanceMoteur
    * PrixVente
    */

    {
        //Attribut de camion
        private string _marque;
        private string _type;
        private DateTime _annee_Fabrication;
        private int _puissance_moteur;
        private int _prix_vente;

        //Constructeur de camion
        public Camion(string marque, string type, string anne_fabrication, string puissance_moteur, string prix_vente)
        {
            Marque = marque;
            Type = type;
            Annee_Fabrication = DateTime.Parse(anne_fabrication + "-01-01");
            Puissance_moteur = int.Parse(puissance_moteur);
            Prix_vente = int.Parse(prix_vente);
        }
        //Constructeur via tableau de string
        public Camion(string[] Attributs_Marque_Type_Annee_Puissance_Prix)
        {
            Marque = Attributs_Marque_Type_Annee_Puissance_Prix[0];
            Type = Attributs_Marque_Type_Annee_Puissance_Prix[1];
            try
            {
            Annee_Fabrication = DateTime.Parse(Attributs_Marque_Type_Annee_Puissance_Prix[2] + "-01-01");
            }
            catch (Exception e)
            {
                Console.WriteLine("Date saisie invalide :\n" + "\n================" + e + "\n================");
            }
            try
            {
            Puissance_moteur = int.Parse(Attributs_Marque_Type_Annee_Puissance_Prix[3]);
            }
            catch (Exception e)
            {
                Console.WriteLine("Puissance saisie invalide :\n" + "\n================" + e + "\n================");
            }
            try
            {
            Prix_vente = int.Parse(Attributs_Marque_Type_Annee_Puissance_Prix[4]);
            }
            catch (Exception e)
            {
                Console.WriteLine("Prix saisi invalide :\n" + "\n================" + e + "\n================");
            }
        }

        public void Affiche()
        {
            Console.WriteLine(Marque +" " +Type);
        }
        #region accesseurs
        public string Marque
        {
            get
            {
                return _marque;
            }
            set
            {
                _marque = value;
            }
        }
        public string Type
        {
            get
            {
                return _type;
            }
            set
            {
                _type = value;
            }
        }
        public DateTime Annee_Fabrication
        {
            get
            {
                return _annee_Fabrication;
            }
            set
            {
                _annee_Fabrication = value;
            }
        }
        public int Puissance_moteur
        {
            get
            {
                return _puissance_moteur;
            }
            set
            {
                _puissance_moteur = value;
            }
        }
        public int Prix_vente
        {
            get
            {
                return _prix_vente;
            }
            set
            {
                _prix_vente = value;
            }
        }
        #endregion accesseurs
    }
}